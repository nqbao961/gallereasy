import axios from 'axios';

export const getImages = async (keyword, limit, offset) => {
    console.log(offset);
    const res = await axios.get(
        `http://api.giphy.com/v1/gifs/search?q=${keyword}&api_key=m87ZgApZ1W9l1mFPIFAOfFCkJSHVVgCL&limit=${limit}&offset=${offset}`
    );
    return res.data;
}