import React, { Component } from 'react';
import AppContext from './AppContext';
import ImageItem from './ImageItem.js';

class FavouriteTab extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <AppContext.Consumer>
                {(context) => {
                    let likedList = null;
                    if (context.liked) {
                        likedList = context.liked.map((item, index) =>
                            <ImageItem onClickImg={context.addLikedImg} id={item.id} key={item.id} src={item.src} alt={item.alt} isLikedClass={'liked'} />)
                    }
                    return <div id="favouritesTab" className="tab-pane fade">
                        <div className="row">
                            {likedList}
                        </div>
                    </div>
                }
                }
            </AppContext.Consumer>

        );
    }
}

export default FavouriteTab;