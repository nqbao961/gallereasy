import React, { Component } from 'react';
import AppContext from './AppContext';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = { value: '' };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  render() {
    return (
      <AppContext.Consumer>
        {(context) =>
          <form onSubmit={context.handleSubmit(this.state.value)}>
            <input type="text" value={this.state.value} onChange={this.handleChange} placeholder={"Start searching for images!"} />
          </form>
        }
      </AppContext.Consumer>
    );
  }
}

export default Search;