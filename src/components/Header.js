import React, { Component } from 'react';
import AppContext from './AppContext';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <AppContext.Consumer>
                {(context) =>
                    <ul className="main-nav nav nav-pills">
                        <li className="logo"><a href="#">Galler<strong>easy</strong></a></li>
                        <li className="active"><a data-toggle="pill" href="#searchTab">Search</a></li>
                        <li><a data-toggle="pill" href="#favouritesTab">Favourites({context.liked ? context.liked.length : 0})</a></li>
                    </ul>
                }
            </AppContext.Consumer>
        );
    }
}

export default Header;