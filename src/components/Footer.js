import React, { Component } from 'react';

class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <div id="footer">
                <p className="alignleft">Gallereasy POC web app</p>
                <p className="alignright">2359 Media</p>
                <div style={{ clear: 'both' }} />
            </div>
        );
    }
}

export default Footer;