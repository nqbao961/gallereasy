import React, { Component } from 'react';
import SearchTab from './SearchTab.js';
import FavouriteTab from './FavouriteTab.js';

class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <div className="tab-content">
                <SearchTab />
                <FavouriteTab />
            </div>
        );
    }
}

export default Content;