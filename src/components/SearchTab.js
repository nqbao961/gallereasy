import React, { Component } from 'react';
import Search from './Search';
import AppContext from './AppContext';
import ImageGroup from './ImageGroup';

class SearchTab extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <AppContext.Consumer>
                {(context) => {
                    let loadMoreBtn = null;
                    if (context.searchStr !== '')
                        loadMoreBtn = <button className="load-more" onClick={context.loadMore}>Load more</button>;
                    return <div id="searchTab" className="tab-pane fade in active">
                        <Search />
                        <ImageGroup />
                        {loadMoreBtn}
                    </div>
                }
                }
            </AppContext.Consumer>
        );
    }
}

export default SearchTab;