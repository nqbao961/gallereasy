import React, { Component } from 'react';
import AppContext from './AppContext';

class ImageItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
        this.handleLike = this.handleLike.bind(this);
    }

    handleLike() {
        this.props.onClickImg(this.props);
    }

    render() {
        return (
            <AppContext.Consumer>
                {(context) => {
                    return <div onClick={this.handleLike} className={`col-md-3 imgContainer ${(context.liked.some(item => item.id === this.props.id)) ? 'liked' : null}`}>
                        <img src={this.props.src} alt={this.props.alt} />
                        <div className="overlay">
                            <span className="icon">
                                <i className="fa fa-heart"></i>
                            </span>
                        </div>
                    </div>
                }}
            </AppContext.Consumer>
        );
    }
}

export default ImageItem;