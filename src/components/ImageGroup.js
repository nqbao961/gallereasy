import React, { Component } from 'react';
import AppContext from './AppContext';
import ImageItem from './ImageItem.js';

class ImageGroup extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <div className="row">
                <AppContext.Consumer>
                    {(context) => {
                        if (context.results) {
                            return context.results.map((item, index) =>
                                <ImageItem onClickImg={context.addLikedImg} id={item.id} key={item.id} src={item.images.original_still.url} alt={item.slug} isLikedClass={''} />)
                        }
                    }}
                </AppContext.Consumer>
            </div>
        );
    }
}

export default ImageGroup;