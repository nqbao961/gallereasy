import React, { Component } from 'react';
import AppContext from './AppContext';
import {getImages} from '../utils/API';
import $ from 'jquery';

class AppProvider extends Component {
    state = {
        liked: [],
        results: [],
        offset: 0,
        searchStr: '',
        handleSubmit: (input) => {
            return (event) => {
                this.setState({ searchStr: input });
                let urlInput = encodeURI(input);
                let limit = 8;
                getImages(urlInput,limit,0).then(data => {
                    this.setState({ results: data.data });
                })
                this.setState({ offset: 8 });
                event.preventDefault();
            }
        },
        loadMore: () => {
            let urlInput = encodeURI(this.state.searchStr);
            let limit = 8;
            getImages(urlInput,limit,this.state.offset).then(data => {
                this.setState({ results: [...this.state.results, ...data.data] });
            })
            this.setState({ offset: this.state.offset + 8 });
        },
        addLikedImg: (imgItem) => {
            if (this.state.liked.some(item => item.id === imgItem.id)) {
                return this.setState({
                    liked: this.state.liked.filter(function (item) {
                        return item.id !== imgItem.id;
                    })
                });
            }
            this.setState({ liked: [...this.state.liked, imgItem] });
        }
    }
    render() {
        return <AppContext.Provider value={this.state}>
            {this.props.children}
        </AppContext.Provider>
    }
}

export default AppProvider;