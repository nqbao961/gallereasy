import React from 'react';
import Header from './components/Header.js';
import Content from './components/Content.js';
import Footer from './components/Footer.js';
import AppProvider from './components/AppProvider';
import './App.css';

function App() {
  return (
    <AppProvider>
      <div className="App">
        <div className="container">
          <Header />
          <Content />
          <Footer />
        </div>
      </div>
    </AppProvider>
  );
}

export default App;
